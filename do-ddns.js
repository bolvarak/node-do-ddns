#!/usr/bin/node
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'use strict'; /// Strict Syntax //////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Module Dependencies //////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// We need our environment
let $environment = require('./environment');
// We need the Request module
let $request = require('request');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// DNS Record Updater ///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * This funciton updates a DNS record in Digital Ocean
 * @name updateDnsRecor()
 * @param {string} $domain
 * @param {number} $id
 * @param {string} $ipv4
 * @param {string} $ipv6
 * @returns {Promise}
 */
let updateDnsRecord = ($domain, $id, $ipv4, $ipv6) => {
	// Return our new promise
	return new Promise(($resolve, $reject) => {
		// Load the record
		$request({
			'method': 'GET',
			'headers': {
				'Authorization': ('Bearer ' + $environment.api.token),
				'Content-Type': 'application/json'
			},
			'url': ('https://api.digitalocean.com/v2/domains/' + $domain + '/records/' + $id),
			'json': true,
		}, ($error, $response, $record) => {
			// Check for an error
			if ($error) {
				// Reject the promise
				return $reject($error);
			}
			// Update the record
			$request.put({
				'headers': {
					'Authorization': ('Bearer ' + $environment.api.token),
					'Content-Type': 'application/json'
				},
				'url': ('https://api.digitalocean.com/v2/domains/' + $domain + '/records/' + $id),
				'json': true,
				'body': {
					'data': (($record.domain_record.type.toLowerCase() === 'a') ? $ipv4 : $ipv6)
				}
			}).on('response', ($response, $body) => {
				// Log the message
				console.log('[' + (($record.domain_record.name === '@') ? '' : $record.domain_record.name + '.') + $domain + ']:\t' + $record.domain_record.data + ' -> ' + (($record.domain_record.type.toLowerCase() === 'a') ? $ipv4 : $ipv6));
				// Resolve the promise
				$resolve();
			}).on('error', ($error) => {
				// Reject the promise
				$reject($error);
			});
		});
	});
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
(() => { /// Main Event Loop /////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Log our separator
	console.log('===============================================================================');
	// Log our message
	console.log('Determining IPv4 and IPv6 Addresses ...');
	// Load the IPv4 address
	return $request.get('http://v4.ipv6-test.com/api/myip.php', ($error, $responsev4, $ipv4) => {
		// Log our separator
		console.log('===============================================================================');
		// Throw the error if one exists
		if ($error) throw $error;
		// Log the IPv4 address
		console.log('[IPv4 Addr]:\t' + $ipv4);
		// Load the IPv6 address
		$request.get('http://v6.ipv6-test.com/api/myip.php', ($error, $responsev6, $ipv6) => {
			// Throw the error if one exists
			if ($error) throw $error;
			// Log the IPv6 address
			console.log('[IPv6 Addr]:\t' + $ipv6);
			// Log our separator
			console.log('===============================================================================');
			// Define our awaitables
			let $await = [];
			// Iterate over the domains in the environment
			for (let $domain in $environment.domains) {
				// Iterate over the records
				$environment.domains[$domain].forEach(($id, $index) => {
					// Add the promise to the awaitables
					$await.push(updateDnsRecord($domain, $id, $ipv4, $ipv6));
				});
			}
			// Await the promises
			Promise.all($await).then(() => {
				console.log('===============================================================================');
				// Log our message
				console.log('Done!');
				// Log our separator
				console.log('===============================================================================');
			}).catch(($error) => {
				// Log the error
				console.error($error);
			});
		});
	});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
})(); /// End Main Event Loop ////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

