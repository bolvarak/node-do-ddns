node-do-ddns
============

This package is a simple script to allow you to use Digital Ocean's domain service as a dynamic DNS provider.


Installation
-------------

```
npm install -g node-do-ddns
```

Configuration
-------------
Configuration is done in `environment.js`, simply copy `environment.example.js` to `environment.js` and modify the values.

| Key                            | Value                                                                                               |
| ------------------------------ | --------------------------------------------------------------------------------------------------- |
| `api`.`token`                  | This is the `token` that you will generate in the [Digital Ocean] (https://digitalocean.com/) UI    |
| `domains`.`<domain-name>`      | `domains` is an object in which the keys are the domains you have hosted in Digital Ocean           |
| `domains`.`<domain-name>[...]` | The value of the `domains`.`<domain-name>` key is an array of record IDs you want updated           |

Example Configuration
---------------------
```json
{
	"api": {
		"token": "SomeSuperSecretToken"
	},
	"domains": {
		"example.com": [
			12345
			67890,
			11121,
			31415,
			16171
		]
	}
```

Service
-------

You can use a cron job below and modify the frequency as needed, or you could use a systemd timer service which you will find below the cron.

```cron
*/30    *       *       *       *       node /path/to/node-do-ddns/do-ddns.js
```

do-ddns.service
---------------
```ini
[Unit]
Description=Digital Ocean DDNS

[Service]
Type=oneshot
ExecStart=/usr/bin/node /path/to/node-do-ddns/do-ddns.js
```

do-ddns.timer
-------------
```ini
[Unit]
Description=Digital Ocean DDNS Updater

[Timer]
OnCalendar=*:0/30
Persistent=true

[Install]
WantedBy=timers.target
```

Enable Systemd
--------------
Copy both files to `/lib/systemd/system` or `/usr/lib/systemd/system` then execute the following to enable the service
```bash
systemctl enable do-ddns.timer && systemctl start do-ddns.timer
```
